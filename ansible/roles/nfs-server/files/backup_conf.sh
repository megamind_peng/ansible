#!/bin/bash
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin

#变量
Host=`hostname`
Ip=`hostname -I|awk '{print $2}'`
Date=`date +%F`
All=${Host}_${Ip}_${Date}
Conf='/usr/local/sersync /etc/exports /script/conf_backup.sh /var/spool/cron/root'

#创建目录
mkdir -p /backup/$All

#打包重要文件，并只保留7天之内的包文件
tar czf /backup/$All/conf_backup.tar.gz $Conf
find /backup -type d -mtime +7 -exec rm -rf {} \;

#做文件md5编码，服务端依据此打包文件的md5编码进行打包文件内容校验
md5sum /backup/$All/conf_backup.tar.gz > /backup/$All/check.md5

#远程数据PUSH
export RSYNC_PASSWORD=1
rsync -az /backup/$All rsync_backup@172.16.1.41::backup
