#!/bin/bash
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin

#只保留60天之内的数据
find /backup -type d -mtime +60 -exec rm -rf {} \;

#进行校验，并将校验结果输出为文件
md5sum -c /backup/*_`date +%F`/check.md5 > /backup/`date +%F`.md5.check

#将校验结果文件发邮件给XXXXX
mail -s "Rsync `date +%F`" XXXXXXXXX@XXX.com < /backup/`date +%F`.md5.check

#校验结果只保留7天之内的
find /backup -type f -mtime +7 -name "*.md5.check" -exec rm -f {} \;
