#!/bin/bash
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin

#变量
Host=`hostname`
Ip=`hostname -I|awk '{print $2}'`
Date=`date +%F`
All=${Host}_${Ip}_${Date}
Conf='/etc/httpd/conf/httpd.conf /script/conf_backup.sh /var/spool/cron/root'

mkdir -p /backup/$All

#打包备份
tar czf /backup/$All/conf_backup.tar.gz $Conf
find /backup -type d -mtime +7 -exec rm -rf {} \;

#文件md5加密
md5sum /backup/$All/conf_backup.tar.gz > /backup/$All/check.md5

#PUSH
export RSYNC_PASSWORD=1
rsync -az /backup/$All rsync_backup@172.16.1.41::backup
